package org.bej2.film.repository;

import org.bej2.film.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * @Repository digunakan supaya Spring mengenali kelas ini sebagai repository
 *      repository diperlukan agar dapat mengakses data pada DB
 * JpaRepository itu library yang dibutuhkan untuk mempermudah akses kita terhadap DB
 *      argument pertama diisi dengan kelas Entity, dan yg kedua diisi dengan tipe data Id dari entity tsb.
 * @Author Aji Nurkholis
 */
@Repository
public interface FilmRepository extends JpaRepository<Film, UUID> {

    /**
     * @Query digunakan untuk membuat syntax SQL sendiri,
     *  nativeQuery di set true jika kita ingin menggunakan syntax yg betul2 sama dengan yg kita gunakan di DB Editor
     */
    @Query(value = "SELECT * FROM film WHERE sedang_tayang = true;", nativeQuery = true)
    List<Film> getFilmSedangTayang();

}
