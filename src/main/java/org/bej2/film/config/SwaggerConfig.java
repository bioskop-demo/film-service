package org.bej2.film.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Configuration , digunakan untuk menandakan kelas ini adalah configuration pada spring boot
 */
@Configuration
public class SwaggerConfig {

    /**
     * @Bean digunakan untuk menandai method ini menjadi bean yang akan dibuat oleh spring
     *  dan disimpan di spring beans container
     * Method dibawah digunakan untuk konfigurasi swagger nya
     */
    @Bean
    public OpenAPI demoApi(@Value("Backend API for Bioskop Use Case") String appDescription,
                           @Value("v1.0.0") String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("Bioskop API")
                        .version(appVersion)
                        .description(appDescription)
                        .termsOfService("http://swagger.io/terms")
                        .license(new License()
                                .name("Apache 2.0")
                                .url("http://springdoc.org")));
    }

}
