package org.bej2.film.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddMovieRequest {
    private String name;
    private MultipartFile img;
    private String schedules;
    private String studio;
}
