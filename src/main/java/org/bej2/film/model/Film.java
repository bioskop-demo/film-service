package org.bej2.film.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @Data digunakan untuk pembuatan setter dan getter yg otomatis by Lombok
 * @AllArgsConstructor untuk membuat method constructor yg menggunakan semua field
 * @NoArgsConstructor untuk membuat method constructor kosongan
 * @Entity digunakan untuk menandakan bahwa kelas ini adalah entity,
 *  maksudnya class ini akan dibuatkan table di DB dengan nama yg sama.
 *  dan field yg sama juga dengan properties nya.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Film {

    /**
     * @Id digunakan untuk menandakan properties ini adalah primary key
     * @GeneratedValue digunakan untuk memasukkan logic auto generator value Id nya,
     *      generator = "UUID" itu diambil dari @GenericGenerator name
     * @GenericGenerator untuk memasukkan logic generator nya
     * @Cascade, agar setiap perubahan pada entity ini akan terubah juga di entity yg memiliki relasi dengan entity ini
     */
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Cascade(CascadeType.ALL)
    private UUID id;

    private String judul;
    private String videoTrailer;
    private String sutradara;

    /**
     * @Lob atau Large Object, digunakan untuk menandakan bahwa property ini memerlukan resource yang besar
     * String sejatinya akan di map menjadi varchar yg memiliki maksimal hanya hingga 255 karakter,
     * sehingga dengan @Lob simpan data stringnya tidak akan dibatasi lagi.
     */
    @Lob
    private String sinopsis;

    private Date tglRilis;
    private Date tglAkhir;
    private Boolean sedangTayang;
    private String genre;
    private Integer minUmur;
    private Date createdAt;
    private Date updatedAt;
    private Double rating;
    private String backPoster;
    private String poster;
    private Long durasi;
}
