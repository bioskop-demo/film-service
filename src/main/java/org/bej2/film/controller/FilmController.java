package org.bej2.film.controller;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.bej2.film.model.Film;
import org.bej2.film.model.request.AddMovieRequest;
import org.bej2.film.model.response.FilmResponse;
import org.bej2.film.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @RestController digunakan untuk java mengenali kelas ini controller Rest API
 * @RequestMapping itu digunakan untuk basePath nya (seluruh endpoint di kelas ini, akan dimulai dari /api/movies
 * @CrossOrigin digunakan untuk selesaikan masalah CORS di FE
 * @Author : BEJ B - Binar Academy Synrgy Batch 5
 */
@RestController
@RequestMapping("/api/movies")
@CrossOrigin("*")
public class FilmController {

    /**
     * autowired digunakan untuk dependency injection
     * dependency injection diperlukan untuk ngambil object yg di autowired dari spring beans container
     * @Author : Agus & Patra
     */
    @Autowired
    FilmService filmService;

    /**
     * untuk HTTP Request dengan Method Get, dan endpoint /get-movies
     * Sehingga di akhir itu menjadi /api/movies/get-movies
     * @return
     */
    @GetMapping("/get-movies")
    public ResponseEntity<List<FilmResponse>> showMovie() {
        return new ResponseEntity<>(filmService.getMovies(), HttpStatus.OK);
    }

    /**
     * consumes mengonfigurasikan API ini hanya menerima MediaType request tertentu
     * MediaType ini menunjukkan format body yg diserahkan oleh client / FE
     * Untuk handle File dan data biasa secara bersamaan kita bisa pake @ModelAttribute dengan membuat model request yg baru
     * tidak lupa tipe data untuk file itu diset sebagai MultipartFile;
     * @param addMovieRequest, berisikan data yg non File dengan data yg File nya
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/add", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String addMovie(@ModelAttribute AddMovieRequest addMovieRequest) throws IOException {
        filmService.addMovies(addMovieRequest);
        return "OK";
    }
}
