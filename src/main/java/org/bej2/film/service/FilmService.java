package org.bej2.film.service;

import org.bej2.film.model.request.AddMovieRequest;
import org.bej2.film.model.response.FilmResponse;

import java.io.IOException;
import java.util.List;

public interface FilmService {

    List<FilmResponse> getMovies();

    void addMovies(AddMovieRequest addMovieRequest) throws IOException;
}
