package org.bej2.film.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.bej2.film.model.Film;
import org.bej2.film.model.request.AddMovieRequest;
import org.bej2.film.model.response.FilmResponse;
import org.bej2.film.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * @Slf4j digunakan agar di kelas ini bisa menggunakan log dengan mudah
 * @Service kita gunakan agar Spring mengenali class ini sebagai service
 * service ini akan membuatkan beans untuk masing2 method nya
 */
@Slf4j
@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    public FilmRepository filmRepository;

    public FilmServiceImpl() {}

    public FilmServiceImpl(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public List<FilmResponse> getMovies() {
        List<FilmResponse> listResponse = new ArrayList<>();
        try {
            List<Film> listData = filmRepository.getFilmSedangTayang();
            if(listData.isEmpty() || listData == null) {
                throw new NullPointerException("Data film yang sedang tayang tidak ditemukan");
            }
            listResponse = this.constructResponse(listData);
        } catch (NullPointerException npe) {
            log.error(npe.getMessage());
        }
        return listResponse;
    }

    /**
     * Logic tambah film harus ada di service tidak boleh ada di controller.
     * Implementasi 3rd party api untuk upload gambar dengan cloudinary
     * @param addMovieRequest, model attribute yg dibawa ke method ini
     * @throws IOException
     */
    @Override
    public void addMovies(AddMovieRequest addMovieRequest) throws IOException {
        /**
         * inisialisasi cloudinary berupa konfigurasi koneksi ke cloudinary nya
         */
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "test-rizky",
                "api_key", "189827629937255",
                "api_secret", "rH3rKU4pY5_xn19cYWQvXR2A8gs"));
        Map<String, Object> uploadResponse = cloudinary.uploader().upload(addMovieRequest.getImg().getBytes(),
                ObjectUtils.asMap("public_id", Optional.ofNullable(addMovieRequest.getName()).orElse("Test Poster")));

        /**
         * DTO data dari addMovieRequest ke object film sebagai entity insert data nanti
         * Set up untuk insert data film ke database
         */
        Film film = new Film();
        film.setJudul(addMovieRequest.getName());
//        film.setJudul("Freedom");


//        film.setPoster(uploadResponse.get("url").toString());
        film.setGenre(addMovieRequest.getStudio());
        film.setSedangTayang(true);
//        film.setTglRilis(new Date(addMovieRequest.getSchedules()));
        filmRepository.save(film);
    }

    /**
     * Buat list filmResponse dari hasil query ke database yg entity nya Film
     * @param filmList, data list film yg diperoleh dari repository film
     * @return List<FilmResponse>, berupa list dengan form film response
     * implementasi method ini merupakan salah satu contoh dari DTO (Data Transfer Object)
     *  yang mengubah entity Film menjadi FilmResponse yg sesuai dengan API Contract yg sudah disetujui
     */
    private List<FilmResponse> constructResponse(List<Film> filmList) {
        // Inisialisasi list untuk response nya
        List<FilmResponse> filmResponses = new ArrayList<>();

        /**
         *  masing-masing filmList, akan di masukkan ke dalam filmResponse sesuai formatnya
         *  formatnya id = id, name = judul, img = poster
         */
        filmList.stream().forEach(f -> {
            filmResponses.add(new FilmResponse(f.getId().toString(),
                    f.getJudul(), f.getPoster()));
        });
        return filmResponses;
    }

}
