package org.bej2.film.service;

import org.bej2.film.model.Film;
import org.bej2.film.model.response.FilmResponse;
import org.bej2.film.repository.FilmRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 *
 */
@SpringBootTest
public class FilmServiceImplTest {

    FilmServiceImpl filmServiceImpl;

    /**
     * Untuk menandakan bahwa object ini adalah object mock
     */
    @Mock
    FilmRepository filmRepository;

    private static final List<Film> listFilmNull = null;
    private static final List<Film> listFilmEmpty = new ArrayList<>();

    /**
     * @BeforeEach menandakan bahwa method ini akan dijalankan setiap sebelum test dijalanin
     */
    @BeforeEach
    public void init() {
        /**
         * Inject filmRepository yang mock ke FilmServiceImpl
         */
        filmServiceImpl = new FilmServiceImpl(this.filmRepository);
    }

    /**
     * Test cases :
     * 1. Input hasil show ke list, list assert apakah dia null, assert jumlah list nya juga (+)
     * 2. get movies ketika berbarengan dengan add movies, assert siapa yg duluan
     * 3. get movies tapi nama movies baru diset (barengan)
     * ==============================================================================================
     * 1. get movies, movies ada, dan jumlah movies berikut isinya sesuai
     * 2. get movies, beberapa sedang tidak tayang, yang ditampilkan hanya sedang tayang
     * 3. get movies, hasil get dari database kosong
     */

    /**
     * @Test untuk menandakan method ybs adalah test case
     */
    @Test
    void getMovies_success() {
        /**
         * Persiapan data expected untuk di assert
         */
        Film film1 = new Film();
        film1.setId(UUID.randomUUID());
        film1.setJudul("test1");
        film1.setPoster("testPoster");

        Film film2 = new Film();
        film2.setId(UUID.randomUUID());
        film2.setJudul("test2");
        film2.setPoster("testPoster");

        Film film3 = new Film();
        film3.setId(UUID.randomUUID());
        film3.setJudul("test3");
        film3.setPoster("testPoster");

        List<Film> filmListExpected = Arrays.asList(film1, film2, film3);

        /**
         * Stubbing data filmListExpected ke hasil jalanin method getFilmSedangTayang nya filmRepository
         * Maksud dari stubbing data disini tidak lain adalah untuk memberikan data untuk hasil ngejalanin method nya
         *   mengingat object yg digunakan adalah object mock.
         */
        when(filmRepository.getFilmSedangTayang()).thenReturn(filmListExpected);

        /**
         * Jalanin method nya sekaligus untuk memvalidasi apa saja hasil yg mesti kita dapat
         */
        List<FilmResponse> result = assertDoesNotThrow(() -> filmServiceImpl.getMovies());
        assertNotNull(result);
        assertNotEquals(0, result.size());
        assertEquals(3, result.size());

        result.forEach(r -> {
            assertNotNull(r.getName());
            assertNotNull(r.getImg());
            assertNotNull(r.getId());
        });
    }

    @Test
    void whenNull_getMovies_failed() {
        when(filmRepository.getFilmSedangTayang()).thenReturn(null);

        List<FilmResponse> result = filmServiceImpl.getMovies();
        assertTrue(result.isEmpty());
    }
}
